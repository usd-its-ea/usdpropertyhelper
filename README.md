# usdpropertyhelper

Small tool for fetching properties from the USD Servers.

**Important:** This utility will only work on servers that have an environment variable called `USDPROPS_HOME` and a 
file at that location called `config.properties`. 

## Installation

The recommended way to install is via Maven. This jar file is available in our local [Nexus 
installation](https://myutil.sandiego.edu:8443/wiki/index.php/Processes/Nexus). 

In your `pom.xml` add the Nexus repository:
```xml
<repositories>
    <repository>
        <id>maven-usd-internal</id>
        <name>maven-usd-internal</name>
        <url>
            https://alfred.sandiego.edu:9443/repository/maven-usd-internal/
        </url>
    </repository>
</repositories>
```

Then add to your project's dependencies (check the GitHub releases to get latest version): 
```xml
<dependency>
    <groupId>edu.sandiego.custom.utils</groupId>
    <artifactId>usd-property-helper</artifactId>
    <version>1.4.0</version>
    <scope>compile</scope>
</dependency>
```

Note that you will have to configure your Maven installation to have the proper credentials to
the Nexus repository. Detailed instructions on how to do that are [here](https://help.sonatype.com/repomanager2/maven-and-other-build-tools/apache-maven).

To get the Nexus Repository username and password, please check Secret Server.


## Usage

```
USDPropertyHelper helper = USDPropertyHelper.getInstance();
String username = helper.getUSDSecurityBannerUser();
String password = helper.getUSDSecurityBannerPassword();
```

## Uploading to Nexus

Just run this maven deploy command: 
`mvn deploy:deploy-file -DgroupId=edu.sandiego.custom.utils -DartifactId=usd-property-helper -Dversion=1.4.0 -Dpackaging=jar -Dfile=usd-property-helper.jar -DrepositoryId=maven-usd-internal -Durl=https://alfred.sandiego.edu:9443/repository/maven-usd-internal/`

**Important**: Remember to increase the version number. Otherwise, you will overwrite the old version.

## Contributing

We always welcome contributions. For guidelines on how to contribute, please refer to the 
[contributing documentation](CONTRIBUTING.md).

## Support

For support, please refer to the [support documentation](SUPPORT.md).

## License

[MIT License](LICENSE.md)