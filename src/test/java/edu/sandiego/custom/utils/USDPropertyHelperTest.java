package edu.sandiego.custom.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import junit.framework.Assert;
import junit.framework.TestCase;

public class USDPropertyHelperTest extends TestCase {

    public void setUp() throws Exception {
        super.setUp();
    }

    public void tearDown() throws Exception {
    }

    public void testGetInstance() {
    }

    public void testGetUSDSecurityClientID() {
    }

    public void testGetERPTAPIDEVProperties() throws JsonProcessingException {
        Assert.assertNotNull(USDPropertyHelper.getInstance().getErptApiProdProperties());
    }

    public void testGetCashnetSSOProperties() throws JsonProcessingException {
        Assert.assertNotNull(new CashnetSSOValues().getEncryptionKey());
    }

    public void testGetUSDSecuritySecret() {
    }

    public void testGetUSDSecurityBannerUser() {
    }

    public void testGetUSDSecurityBannerPassword() {
    }

    public void testGetMySanDiegoEnv() {
    }

    public void testGetSalesforceEnv() {
    }
}