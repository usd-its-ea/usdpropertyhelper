package edu.sandiego.custom.utils;

/**
 * Created by rdhiman on Dec.20.2018.
 */
public class CashnetSSOValues {

    /** Cashnet SSO Base URL.*/
    private String baseUrl;
    /** Cashnet SSO Encryption Key.*/
    private String encryptionKey;

    /**
     * No args constructor. Bootstrapping all the values at the time of object creation.
     */
    public CashnetSSOValues() {
        USDPropertyHelper usdPropertyHelper = USDPropertyHelper.getInstance();
        this.baseUrl = usdPropertyHelper.getProperty(CashnetSSOProperties.CASHNET_SSO_BASE_URL);
        this.encryptionKey = usdPropertyHelper.getProperty(CashnetSSOProperties.CASHNET_SSO_ENCRYPTION_KEY);
    }

    /**
     * Get Base Url.
     * @return : String
     */
    public final String getBaseUrl() {
        return baseUrl;
    }

    /**
     * Get Encryption Key.
     * @return : String
     */
    public final String getEncryptionKey() {
        return encryptionKey;
    }

}
