package edu.sandiego.custom.utils;

/**
 * Created by rdhiman on Sep.26.2018.
 */
public class ERPTAPIPRODValues {

    /** ERPT API DEV Base URL.*/
    private String baseUrl;
    /** ERPT API DEV Version. */
    private String version;
    /** ERPT API DEV Salesforce Path.*/
    private String salesforcePath;
    /** ERPT API DEV BannerPath. */
    private String bannerPath;
    /** ERPT API DEV Firebase Path. */
    private String firebasePath;
    /** ERPT API DEV Google Path. */
    private String googlePath;
    /** ERPT API DEV Twilio Path.*/
    private String twilioPath;
    /** ERPT API DEV Send Grid Path.*/
    private String sendgridPath;
    /** ERPT API DEV Public path.*/
    private String publicPath;

    /**
     * No args constructor. Bootstrapping all the values at the time of object creation.
     */
    public ERPTAPIPRODValues() {

        USDPropertyHelper usdPropertyHelper = USDPropertyHelper.getInstance();
        this.baseUrl = usdPropertyHelper.getProperty(ERPTAPIPRODProperties.USD_ERPT_PROD_API_BASE_URL);
        this.version = usdPropertyHelper.getProperty(ERPTAPIPRODProperties.USD_ERPT_PROD_API_VERSION);
        this.salesforcePath = usdPropertyHelper.getProperty(ERPTAPIPRODProperties.USD_ERPT_PROD_API_SALESFORCE_PATH);
        this.bannerPath = usdPropertyHelper.getProperty(ERPTAPIPRODProperties.USD_ERPT_PROD_API_BANNER_PATH);
        this.firebasePath = usdPropertyHelper.getProperty(ERPTAPIPRODProperties.USD_ERPT_PROD_API_FIREBASE_PATH);
        this.googlePath = usdPropertyHelper.getProperty(ERPTAPIPRODProperties.USD_ERPT_PROD_API_GOOGLE_PATH);
        this.twilioPath = usdPropertyHelper.getProperty(ERPTAPIPRODProperties.USD_ERPT_PROD_API_TWILIO_PATH);
        this.sendgridPath = usdPropertyHelper.getProperty(ERPTAPIPRODProperties.USD_ERPT_PROD_API_SENDGRID_PATH);
        this.publicPath = usdPropertyHelper.getProperty(ERPTAPIPRODProperties.USD_ERPT_PROD_API_PUBLIC_PATH);
    }

    /**
     * Get Base Url.
     * @return :String
     */
    public final String getBaseUrl() {
        return baseUrl;
    }

    /**
     *  Get Version.
     * @return :String
     */
    public final String getVersion() {
        return version;
    }

    /**
     *
     * @return :String
     */
    public final String getSalesforcePath() {
        return salesforcePath;
    }

    /**
     *
     * @return :String
     */
    public final String getBannerPath() {
        return bannerPath;
    }

    /**
     *  Get Firebase Path.
     * @return :String
     */
    public final String getFirebasePath() {
        return firebasePath;
    }

    /**
     *  Get Google Path.
     * @return :String
     */
    public final String getGooglePath() {
        return googlePath;
    }

    /**
     * Get Twilio Path.
     * @return :String
     */
    public final String getTwilioPath() {
        return twilioPath;
    }

    /**
     *  Get Sendrid Path.
     * @return :String
     */
    public final String getSendgridPath() {
        return sendgridPath;
    }

    /**
     * Get Public Path.
     * @return :String
     */
    public final String getPublicPath() {
        return publicPath;
    }
}
