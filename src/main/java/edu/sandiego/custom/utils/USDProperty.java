package edu.sandiego.custom.utils;

/** List of supported USD Properties. */
final class USDProperty {

    /* USD Security Properties. */

    /** USDSECURITY_CLIENTID_NAME. */
    static final String USDSECURITY_CLIENTID_NAME = "usdsecurity.oauth.client-id";
    /** USDSECURITY_SECRET_NAME. */
    static final String USDSECURITY_SECRET_NAME = "usdsecurity.oauth.secret";
    /** USDSECURITY_TOKEN_VALIDITY. */
    static final String USDSECURITY_TOKEN_VALIDITY_SECONDS = "usdsecurity.oauth.token-validity-seconds";
    /** USDSECURITY_BANNERUSER_NAME. */
    static final String USDSECURITY_BANNERUSER_NAME = "usdsecurity.banner.user";
    /** USDSECURITY_BANNERPASSWORD_NAME. */
    static final String USDSECURITY_BANNERPASSWORD_NAME = "usdsecurity.banner.password";

    /* MySanDiego Properties. */

    /** MYSANDIEGO_ENV_NAME. */
    static final String MYSANDIEGO_ENV_NAME = "mysandiego.env";

    /* Mule Properties. */

    /** MULE_BASE_URL. */
    static final String MULE_BASE_URL = "mule.prod.baseURL";
    /** MULE_HOURS_URL. */
    static final String MULE_HOURS_URL = "mule.prod.hoursURL";
    /** MULE_HOURS_REDIRECT_URI. */
    static final String MULE_HOURS_REDIRECT_URI = "mule.prod.redirectURI";
    /** MULE_HOURS_USER. */
    static final String MULE_HOURS_USER = "mule.prod.user";
    /** MULE_HOURS_PASS. */
    static final String MULE_HOURS_PASS = "mule.prod.password";

    /* Salesforce Properties. */

    /** SALESFORCE_ENV_NAME. */
    static final String SALESFORCE_ENV_NAME = "salesforce.env";
    /** SALESFORCE_SERVICE_DATA_URL. */
    static final String SALESFORCE_SERVICE_DATAURL = "salesforce.service.data-url";
    /** SALESFORCE_AUTH_URL. */
    static final String SALESFORCE_AUTH_URL = "salesforce.auth-url";
    /** SALESFORCE_TOKEN_URL. */
    static final String SALESFORCE_TOKEN_URL = "salesforce.token-url";
    /** SALESFORCE_REVOKE_URL. */
    static final String SALESFORCE_REVOKE_URL = "salesforce.revoke-url";

    /** SALESFORCE_PROD_API_VERSION. */
    static final String SALESFORCE_PROD_API_VERSION = "salesforce.prod.api.version";
    /** SALESFORCE_PROD_USER. */
    static final String SALESFORCE_PROD_USER = "salesforce.prod.user";
    /** SALESFORCE_PROD_PASSWORD. */
    static final String SALESFORCE_PROD_PASSWORD = "salesforce.prod.password";
    /** SALESFORCE_PROD_SECRET. */
    static final String SALESFORCE_PROD_SECRET = "salesforce.prod.secret";
    /** SALESFORCE_PROD_CLIENT_ID. */
    static final String SALESFORCE_PROD_CLIENT_ID = "salesforce.prod.client-id";
    /** SALESFORCE_PROD_BASE_URL. */
    static final String SALESFORCE_PROD_BASE_URL = "salesforce.prod.base-url";
    /** SALESFORCE_PROD_REG_URL. */
    static final String SALESFORCE_PROD_REG_URL = "salesforce.prod.reg-url";

    /** SALESFORCE_DEV_API_VERSION. */
    static final String SALESFORCE_DEV_API_VERSION = "salesforce.dev.api.version";
    /** SALESFORCE_DEV_USER. */
    static final String SALESFORCE_DEV_USER = "salesforce.dev.user";
    /** SALESFORCE_DEV_PASSWORD. */
    static final String SALESFORCE_DEV_PASSWORD = "salesforce.dev.password";
    /** SALESFORCE_DEV_SECRET. */
    static final String SALESFORCE_DEV_SECRET = "salesforce.dev.secret";
    /** SALESFORCE_DEV_CLIENT_ID. */
    static final String SALESFORCE_DEV_CLIENT_ID = "salesforce.dev.client-id";
    /** SALESFORCE_DEV_BASE_URL. */
    static final String SALESFORCE_DEV_BASE_URL = "salesforce.dev.base-url";
    /** SALESFORCE_DEV_REG_URL. */
    static final String SALESFORCE_DEV_REG_URL = "salesforce.dev.reg-url";

    /* Amazon Properties. */

    /** AMAZON SKILL ID (Used with USDAlexa). */
    static final String AMZN_SKILL_ID = "amzn.skill-id";

    /** USD Tram Properties. */
    static final String USD_TRAM_API_KEY = "syncromatics.api.key";

    /* EverFi PROD Properties. */

    /** EverFi Production API Token. */
    static final String EVERFI_API_TOKEN = "everfi.api-token";
    /** EverFi Production School Id. */
    static final String EVERFI_SCHOOL_ID = "everfi.school-id";
    /** Everfi Prod Base Url. */
    static final String EVERFI_BASE_URL = "everfi.base-url";
    /** EverFi Prod Service Path. */
    static final String EVERFI_SERVICE_PATH = "everfi.service-path";

    /* LDAP Properties. */

    /** USD LDAP URL. */
    static final String LDAP_URL = "ldap.url";
    /** USD LDAP Manager DN. */
    static final String LDAP_MANAGER_DN = "ldap.manager-dn";
    /** USD LDAP Manager Password. */
    static final String LDAP_MANAGER_PWD = "ldap.manager-pwd";
    /** USD LDAP User DN Pattern. */
    static final String LDAP_USER_DN_PATTERN = "ldap.user-dn-pattern";
    /** USD LDAP User Search Base. */
    static final String LDAP_USER_SEARCH_BASE = "ldap.user-search-base";
    /** USD LDAP User Search Filter. */
    static final String LDAP_USER_SEARCH_FILTER = "ldap.user-search-filter";
    /** USD LDAP Group Search Base. */
    static final String LDAP_GROUP_SEARCH_BASE = "ldap.group-search-base";
    /** USD LDAP Group Role Attribute. */
    static final String LDAP_GROUP_ROLE_ATTRIBUTE = "ldap.group-role-attribute";
    /** USD LDAP Group Search Filter. */
    static final String LDAP_GROUP_SEARCH_FILTER = "ldap.group-search-filter";

    /* SendGrid Properties. */

    /** SendGrid MySanDiego API Key. */
    static final String SENDGRID_MYSANDIEGO_API_KEY = "sendgrid.mysandiego.api.key";
    /** SendGrid MySDMobile API Key. */
    static final String SENDGRID_MYSDMOBILE_API_KEY = "sendgrid.mysdmobile.api.key";
    /** SendGrid LiftUp API Key. */
    static final String SENDGRID_LIFTUP_API_KEY = "sendgrid.liftup.api.key";
    /** SendGrid USDChat API Key. */
    static final String SENDGRID_USDCHAT_API_KEY = "sendgrid.usdchat.api.key";

    /* Twilio Properties. */

    /** Twilio Account SID. */
    static final String TWILIO_ACCOUNT_SID = "twilio.account.sid";
    /** Twilio Auth Token. */
    static final String TWILIO_AUTH_TOKEN = "twilio.auth.token";
    /** Twilio API Key. */
    static final String TWILIO_API_KEY = "twilio.api.key";
    /** Twilio API Secret. */
    static final String TWILIO_API_SECRET = "twilio.api.secret";
    /** Twilio Service USD Test Distribution SID. */
    static final String TWILIO_SERVICE_USDTEST_DISTRIBUTION_SID = "twilio.service.usdtest.distribution.sid";
    /** Twilio Service USD Chat Distribution SID. */
    static final String TWILIO_SERVICE_USDCHAT_DISTRIBUTION_SID = "twilio.service.usdchat.distribution.sid";
    /** Twilio Push USD Test Distribution SID. */
    static final String TWILIO_PUSH_USDTEST_DISTRIBUTION_SID = "twilio.push.usdtest.distribution.sid";
    /** Twilio Push USD Test Development SID. */
    static final String TWILIO_PUSH_USDTEST_DEVELOPMENT_SID = "twilio.push.usdtest.development.sid";
    /** Twilio Push USD Chat Distribution SID. */
    static final String TWILIO_PUSH_USDCHAT_DISTRIBUTION_SID = "twilio.push.usdchat.distribution.sid";
    /** Twilio Push USD Chat Development SID. */
    static final String TWILIO_PUSH_USDCHAT_DEVELOPMENT_SID = "twilio.push.usdchat.development.sid";

    /** Cognos Content Management URL. */
    static final String COGNOS_CONTENT_MANAGEMENT_URL = "cognos.url";
    /** Cognos Content Management Service User Namespace. */
    static final String COGNOS_CONTENT_MANAGEMENT_SERVICE_NAMESPACE = "cognos.namespace";
    /** Cognos Content Management Service User Username. */
    static final String COGNOS_CONTENT_MANAGEMENT_SERVICE_USERNAME = "cognos.username";
    /** Cognos Content Management Service User Password. */
    static final String COGNOS_CONTENT_MANAGEMENT_SERVICE_PASSWORD = "cognos.password";
    /** Cognos Content Management Report Path. */
    static final String COGNOS_CONTENT_MANAGEMENT_REPORT_PATH = "cognos.report-path";

    /** OIM URL for USD One endpoints. */
    static final String USDONE_OIM_URL = "usdone.url";
    /** OIM username for USD One endpoints. */
    static final String USDONE_OIM_USERNAME = "usdone.username";
    /** OIM password for USD One endpoints. */
    static final String USDONE_OIM_PASSWORD = "usdone.password";

    /** Notification firebase service file name. */
    static final String NOTIFICATION_FIREBASE_SERVICE_FILE = "notification.firebase.service-file";
    /** Notification firebase database URL. */
    static final String NOTIFICATION_FIREBASE_URL = "notification.firebase.url";
    /** Notification firebase reference location. */
    static final String NOTIFICATION_FIREBASE_REFERENCE_LOCATION = "notification.firebase.reference-location";

    /** API key for a JWT App on Zoom. */
    static final String ZOOM_API_KEY = "zoom.api-key";
    /** API secret for a JWT App on Zoom. */
    static final String ZOOM_API_SECRET = "zoom.api-secret";

    /** Indicator for whether to run zoom attendance. */
    static final String SCHEDULED_ZOOM_ATTENDANCE = "scheduled.zoom-attendance";

    /** CAS base URL. */
    static final String CAS_BASE_URL = "cas.url";
    /** CAS client ID. */
    static final String CAS_CLIENT_ID = "cas.client-id";
    /** CAS client secret. */
    static final String CAS_CLIENT_SECRET = "cas.client-secret";
   
    /** NUPark base url */
    static final String NU_PARK_BASE_URL = "nupark.url";
    /** NUPark username */
    static final String NU_PARK_USERNAME = "nupark.username";
    /** NUPark password*/
    static final String NU_PARK_PASSWORD = "nupark.password";
    
    /** Prevent this Utility class from being instantiated. */
    private USDProperty() { }

}
