package edu.sandiego.custom.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * USDPropertyHelper.
 * Created by rommel on 10/17/17.
 */
public final class USDPropertyHelper {

    /* Static Properties */

    /** Singleton. */
    private static USDPropertyHelper instance = null;
    /**
     * Location of Server properties.
     * IMPORTANT: This REQUIRES an environment variable called "USDPROPS_HOME"
     * to be created in the environment that this application is running.
     */
    private static final String PROP_HOME = System.getenv("USDPROPS_HOME");
    /** Location of File with properties. */
    private static String propFileLocation = PROP_HOME + "/config.properties";
    /** Logger. */
    private static Logger log =
            LogManager.getLogger(USDPropertyHelper.class.getName());

    /* Constructors */

    /** Exists only to defeat instantiation (i.e. This is a Singleton). */
    private USDPropertyHelper() { }

    /**
     * Get USDPropertyHelper instance.
     * @return USDPropertyHelper singleton.
     */
    public static USDPropertyHelper getInstance() {
        if (instance == null) {
            instance = new USDPropertyHelper();
        }
        return instance;
    }

    /* Getters */

    /**
     * Get ERPT API DEV Properties.
     * @return JSON as String.
     * @throws JsonProcessingException : JsonProcessingException
     */
    public String getErptApiDevProperties() throws JsonProcessingException {

        ERPTAPIDEVValues erptapidevValues = new ERPTAPIDEVValues();
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(erptapidevValues);

    }

    /**
     * Get ERPT API PROD Properties.
     * @return JSON as String.
     * @throws JsonProcessingException : JsonProcessingException
     */
    public String getErptApiProdProperties() throws JsonProcessingException {

        ERPTAPIPRODValues erptapiprodValues = new ERPTAPIPRODValues();
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(erptapiprodValues);
    }
    /**
     * Returns the USD Security Client ID.
     * @return The Client ID or null if there was an error.
     */
    public String getUSDSecurityClientID() {
        return getProperty(USDProperty.USDSECURITY_CLIENTID_NAME);
    }

    /**
     * Returns the USD Security Secret.
     * @return The Secret or null if there was an error.
     */
    public String getUSDSecuritySecret() {
        return getProperty(USDProperty.USDSECURITY_SECRET_NAME);
    }

    /**
     * Returns the USD Token Validity Seconds.
     * @return The validity or null if there was an error.
     */
    public String getUSDSecurityTokenValiditySeconds() {
        return getProperty(USDProperty.USDSECURITY_TOKEN_VALIDITY_SECONDS);
    }

    /**
     * Returns the USD Security Banner User.
     * @return The Banner User or null if there was an error.
     */
    public String getUSDSecurityBannerUser() {
        return getProperty(USDProperty.USDSECURITY_BANNERUSER_NAME);
    }

    /**
     * Returns the USD Security Banner Password.
     * @return The Banner Password or null if there was an error.
     */
    public String getUSDSecurityBannerPassword() {
        return getProperty(USDProperty.USDSECURITY_BANNERPASSWORD_NAME);
    }

    /**
     * Returns the MySanDiego Environment (i.e. "prod" or "dev")
     * @return The MySanDiego environment, or null if there was an error.
     */
    public String getMySanDiegoEnv() {
        return getProperty(USDProperty.MYSANDIEGO_ENV_NAME);
    }

    /**
     * Returns the Salesforce Environment (i.e. "prod" or "appjp")
     * @return The Salesforce environment, or null if there was an error.
     */
    public String getSalesforceEnv() {
        return getProperty(USDProperty.SALESFORCE_ENV_NAME);
    }

    /**
     * Returns the Amazon Skill ID.
     * @return The Amazon skill id, or null if there was an error.
     */
    public String getAmznSkillId() {
        return getProperty(USDProperty.AMZN_SKILL_ID);
    }

    /**
     * Returns the Mule Base URL.
     * @return Mule Base URL String, or null if there was an error.
     */
    public String getMuleBaseUrl() {
        return getProperty(USDProperty.MULE_BASE_URL);
    }

    /**
     * Returns the Mule Hours URL.
     * @return Hours URL String, or null if there was an error.
     */
    public String getMuleHoursUrl() {
        return getProperty(USDProperty.MULE_HOURS_URL);
    }

    /**
     * Returns the Mule Hours Redirect URI.
     * @return Mule Hours Redirect URI String, or null if there was an error.
     */
    public String getMuleHoursRedirectUri() {
        return getProperty(USDProperty.MULE_HOURS_REDIRECT_URI);
    }

    /**
     * Returns the Mule Hours Username.
     * @return Mule Hours User String, or null if there was an error.
     */
    public String getMuleHoursUser() {
        return getProperty(USDProperty.MULE_HOURS_USER);
    }

    /**
     * Returns the Mule Hours Password.
     * @return Mule Hours Password String, or null if there was an error.
     */
    public String getMuleHoursPass() {
        return getProperty(USDProperty.MULE_HOURS_PASS);
    }

    /**
     * Returns the Salesforce Service Data URL.
     * @return Salesforce Service Data URL String, or null if nothing found.
     */
    public String getSalesforceServiceDataURL() {
        return getProperty(USDProperty.SALESFORCE_SERVICE_DATAURL);
    }

    /**
     * Returns the Salesforce Auth URL.
     * @return Salesforce Auth URL String, or null if nothing found.
     */
    public String getSalesforceAuthURL() {
        return getProperty(USDProperty.SALESFORCE_AUTH_URL);
    }

    /**
     * Returns the Salesforce Token URL.
     * @return Salesforce Token URL String, or null if nothing found.
     */
    public String getSalesforceTokenURL() {
        return getProperty(USDProperty.SALESFORCE_TOKEN_URL);
    }

    /**
     * Returns the Salesforce Revoke URL.
     * @return Salesforce Service Data URL String, or null if nothing found.
     */
    public String getSalesforceRevokeURL() {
        return getProperty(USDProperty.SALESFORCE_REVOKE_URL);
    }

    /**
     * Returns the Salesforce Prod API version.
     * @return Salesforce Prod API version String, or null if nothing found.
     */
    public String getSalesforceProdAPIVersion() {
        return getProperty(USDProperty.SALESFORCE_PROD_API_VERSION);
    }

    /**
     * Returns the Salesforce Prod Username.
     * @return Salesforce Prod Username String, or null if nothing found.
     */
    public String getSalesforceProdUser() {
        return getProperty(USDProperty.SALESFORCE_PROD_USER);
    }

    /**
     * Returns the Salesforce Prod Password.
     * @return Salesforce Prod Password String, or null if nothing found.
     */
    public String getSalesforceProdPassword() {
        return getProperty(USDProperty.SALESFORCE_PROD_PASSWORD);
    }

    /**
     * Returns the Salesforce Prod Secret.
     * @return Salesforce Prod Secret String, or null if nothing found.
     */
    public String getSalesforceProdSecret() {
        return getProperty(USDProperty.SALESFORCE_PROD_SECRET);
    }

    /**
     * Returns the Salesforce Prod Client Id.
     * @return Salesforce Prod Client ID String, or null if nothing found.
     */
    public String getSalesforceProdClientId() {
        return getProperty(USDProperty.SALESFORCE_PROD_CLIENT_ID);
    }

    /**
     * Returns the Salesforce Prod Base URL.
     * @return Salesforce Prod Base URL String, or null if nothing found.
     */
    public String getSalesforceProdBaseURL() {
        return getProperty(USDProperty.SALESFORCE_PROD_BASE_URL);
    }

    /**
     * Returns the Salesforce Prod Registration URL.
     * @return Salesforce Prod Reg URL String, or null if nothing found.
     */
    public String getSalesforceProdRegURL() {
        return getProperty(USDProperty.SALESFORCE_PROD_REG_URL);
    }

    /**
     * Returns the Salesforce Dev API version.
     * @return Salesforce Dev API version String, or null if nothing found.
     */
    public String getSalesforceDevAPIVersion() {
        return getProperty(USDProperty.SALESFORCE_DEV_API_VERSION);
    }

    /**
     * Returns the Salesforce Dev Username.
     * @return Salesforce Dev Username String, or null if nothing found.
     */
    public String getSalesforceDevUser() {
        return getProperty(USDProperty.SALESFORCE_DEV_USER);
    }

    /**
     * Returns the Salesforce Dev Password.
     * @return Salesforce Dev Password String, or null if nothing found.
     */
    public String getSalesforceDevPassword() {
        return getProperty(USDProperty.SALESFORCE_DEV_PASSWORD);
    }

    /**
     * Returns the Salesforce Dev Secret.
     * @return Salesforce Dev Secret String, or null if nothing found.
     */
    public String getSalesforceDevSecret() {
        return getProperty(USDProperty.SALESFORCE_DEV_SECRET);
    }

    /**
     * Returns the Salesforce Dev Client Id.
     * @return Salesforce Dev Client ID String, or null if nothing found.
     */
    public String getSalesforceDevClientId() {
        return getProperty(USDProperty.SALESFORCE_DEV_CLIENT_ID);
    }

    /**
     * Returns the Salesforce Dev Base URL.
     * @return Salesforce Dev Base URL String, or null if nothing found.
     */
    public String getSalesforceDevBaseURL() {
        return getProperty(USDProperty.SALESFORCE_DEV_BASE_URL);
    }

    /**
     * Returns the Salesforce Dev Registration URL.
     * @return Salesforce Dev Reg URL String, or null if nothing found.
     */
    public String getSalesforceDevRegURL() {
        return getProperty(USDProperty.SALESFORCE_DEV_REG_URL);
    }

    /**
     * Returns the USD Tram (Syncromatics) API Key.
     * @return USD Tram API Key as a String, or null if nothing found.
     */
    public String getTramAPIKey() {
        return getProperty(USDProperty.USD_TRAM_API_KEY);
    }

    /**
     * Get EverFi API Token  .
     * @return EverFi Prod API Token Value.
     */
    public String getEverfiAPIToken() {
        return getProperty(USDProperty.EVERFI_API_TOKEN);
    }
    /**
     * Get EverFi School Id .
     * @return EverFi School Id Value.
     */
    public String getEverfiSchoolId() {
        return getProperty(USDProperty.EVERFI_SCHOOL_ID);
    }

    /**
     * Get EverFi Base Url .
     * @return Everfi Base Url for Prod.
     */
    public String getEverfiBaseUrl() {
        return getProperty(USDProperty.EVERFI_BASE_URL);
    }

    /**
     * Get EverFi Service .
     * @return Everfi Service Path for Prod.
     */
    public String getEverfiServicePath() {
        return getProperty(USDProperty.EVERFI_SERVICE_PATH);
    }

    /**
     * Get USD LDAP URL.
     * @return USD LDAP URL.
     */
    public String getLdapUrl() {
        return getProperty(USDProperty.LDAP_URL);
    }

    /**
     * Get USD Manager DN.
     * @return USD LDAP Manager DN.
     */
    public String getLdapManagerDN() {
        return getProperty(USDProperty.LDAP_MANAGER_DN);
    }

    /**
     * Get USD LDAP Manager Pwd.
     * @return USD LDAP Manager Pwd.
     */
    public String getLdapManagerPwd() {
        return getProperty(USDProperty.LDAP_MANAGER_PWD);
    }

    /**
     * Get USD LDAP User DN Pattern.
     * @return USD LDAP User DN Pattern.
     */
    public String getLdapUserDNPattern() {
        return getProperty(USDProperty.LDAP_USER_DN_PATTERN);
    }

    /**
     * Get USD LDAP User Search Base.
     * @return USD LDAP User Search Base.
     */
    public String getLdapUserSearchBase() {
        return getProperty(USDProperty.LDAP_USER_SEARCH_BASE);
    }

    /**
     * Get USD LDAP User Search Filter.
     * @return USD LDAP User Search Filter.
     */
    public String getLdapUserSearchFilter() {
        return getProperty(USDProperty.LDAP_USER_SEARCH_FILTER);
    }

    /**
     * Get USD LDAP Group Search Base.
     * @return USD LDAP Group Search Base.
     */
    public String getLdapGroupSearchBase() {
        return getProperty(USDProperty.LDAP_GROUP_SEARCH_BASE);
    }

    /**
     * Get USD LDAP Group Role Attribute.
     * @return USD LDAP Group Role Attribute.
     */
    public String getLdapGroupRoleAttribute() {
        return getProperty(USDProperty.LDAP_GROUP_ROLE_ATTRIBUTE);
    }

    /**
     * Get USD LDAP Group Search Filter.
     * @return USD LDAP Group Search Filter.
     */
    public String getLdapGroupSearchFilter() {
        return getProperty(USDProperty.LDAP_GROUP_SEARCH_FILTER);
    }

    /**
     * Get SendGrid MySanDiego API Key.
     * @return SendGrid MySanDiego API Key.
     */
    public String getSendGridMySanDiegoAPIKey() {
        return getProperty(USDProperty.SENDGRID_MYSANDIEGO_API_KEY);
    }

    /**
     * Get SendGrid MySDMobile API Key.
     * @return SendGrid MySDMobile API Key.
     */
    public String getSendGridMySDMobileAPIKey() {
        return getProperty(USDProperty.SENDGRID_MYSDMOBILE_API_KEY);
    }

    /**
     * Get SendGrid LiftUp API Key.
     * @return SendGrid LiftUp API Key.
     */
    public String getSendGridLiftUpAPIKey() {
        return getProperty(USDProperty.SENDGRID_LIFTUP_API_KEY);
    }

    /**
     * Get SendGrid USD Chat API Key.
     * @return SendGrid USD Chat API Key.
     */
    public String getSendGridUSDChatAPIKey() {
        return getProperty(USDProperty.SENDGRID_USDCHAT_API_KEY);
    }

    /**
     * Get Twilio Account SID.
     * @return Twilio Account SID.
     */
    public String getTwilioAccountSID() {
        return getProperty(USDProperty.TWILIO_ACCOUNT_SID);
    }

    /**
     * Get Twilio Auth Token.
     * @return Twilio Auth Token.
     */
    public String getTwilioAuthToken() {
        return getProperty(USDProperty.TWILIO_AUTH_TOKEN);
    }

    /**
     * Get Twilio API Key.
     * @return Twilio API Key.
     */
    public String getTwilioAPIKey() {
        return getProperty(USDProperty.TWILIO_API_KEY);
    }

    /**
     * Get Twilio API Secret.
     * @return Twilio API Secret
     */
    public String getTwilioAPISecret() {
        return getProperty(USDProperty.TWILIO_API_SECRET);
    }

    /**
     * Get Twilio Service USD Test Distribution SID.
     * @return Twilio Service USD Test Distribution SID.
     */
    public String getTwilioServiceUSDTestDistributionSID() {
        return getProperty(USDProperty.TWILIO_SERVICE_USDTEST_DISTRIBUTION_SID);
    }

    /**
     * Get Twilio Service USD Chat Distribution SID.
     * @return Twilio Service USD Chat Distribution SID.
     */
    public String getTwilioServiceUSDChatDistributionSID() {
        return getProperty(USDProperty.TWILIO_SERVICE_USDCHAT_DISTRIBUTION_SID);
    }

    /**
     * Get Twilio Push USD Test Distribution SID.
     * @return Twilio Push USD Test Distribution SID.
     */
    public String getTwilioPushUSDTestDistributionSID() {
        return getProperty(USDProperty.TWILIO_PUSH_USDTEST_DISTRIBUTION_SID);
    }

    /**
     * Get Twilio Push USD Test Development SID.
     * @return Twilio Push USD Test Development SID.
     */
    public String getTwilioPushUSDTestDevelopmentSID() {
        return getProperty(USDProperty.TWILIO_PUSH_USDTEST_DEVELOPMENT_SID);
    }

    /**
     * Get Twilio Push USD Chat Distribution SID.
     * @return Twilio Push USD Chat Distribution SID.
     */
    public String getTwilioPushUSDChatDistributionSID() {
        return getProperty(USDProperty.TWILIO_PUSH_USDCHAT_DISTRIBUTION_SID);
    }

    /**
     * Get Twilio Push USD Chat Development SID.
     * @return Twilio Push USD Chat Development SID.
     */
    public String getTwilioPushUSDChatDevelopmentSID() {
        return getProperty(USDProperty.TWILIO_PUSH_USDCHAT_DEVELOPMENT_SID);
    }

    /**
     * Get Cognos Content Management URL
     * @return Cognos Content Managent URL
     */
    public String getCognosContentManagementURL() {
        return getProperty(USDProperty.COGNOS_CONTENT_MANAGEMENT_URL);
    }

    /**
     * Get Cognos Content Management Service User Namespace
     * @return Cognos Content Management Service User Namespace
     */
    public String getCognosContentManagementServiceNamespace() {
        return getProperty(USDProperty.COGNOS_CONTENT_MANAGEMENT_SERVICE_NAMESPACE);
    }

    /**
     * Get Cognos Content Management Service User Username
     * @return Cognos Content Management Service User Username
     */
    public String getCognosContentManagementServiceUsername() {
        return getProperty(USDProperty.COGNOS_CONTENT_MANAGEMENT_SERVICE_USERNAME);
    }

    /**
     * Get Cognos Content Management Service User Password
     * @return Cognos Content Management Service User Password
     */
    public String getCognosContentManagementServicePassword() {
        return getProperty(USDProperty.COGNOS_CONTENT_MANAGEMENT_SERVICE_PASSWORD);
    }

    /**
     * Get Cognos Content Management Report Path
     * @return Cognos Content Management Report Path
     */
    public String getCognosContentManagementReportPath() {
        return getProperty(USDProperty.COGNOS_CONTENT_MANAGEMENT_REPORT_PATH);
    }

    /**
     * Get USD One OIM URL
     * @return USD One OIM URL
     */
    public String getUsdOneOimUrl() {
        return getProperty(USDProperty.USDONE_OIM_URL);
    }

    /**
     * Get USD One OIM username
     * @return USD One OIM username
     */
    public String getUsdOneOimUsername() {
        return getProperty(USDProperty.USDONE_OIM_USERNAME);
    }

    /**
     * Get USD One OIM password
     * @return USD One OIM password
     */
    public String getUsdOneOimPassword() {
        return getProperty(USDProperty.USDONE_OIM_PASSWORD);
    }

    /**
     * Get the firebase service file name for the notification configuration.
     * @return The file name of the service file.
     */
    public String getNotificationFirebaseServiceFile() {
        return getProperty(USDProperty.NOTIFICATION_FIREBASE_SERVICE_FILE);
    }

    /**
     * Get the firebase database URL for the notification configuration.
     * @return The database URL.
     */
    public String getNotificationFirebaseDatabaseUrl() {
        return getProperty(USDProperty.NOTIFICATION_FIREBASE_URL);
    }

    /**
     * Get the firebase reference location for the notification configuration.
     * @return The reference location.
     */
    public String getNotificationFirebaseReferenceLocation() {
        return getProperty(USDProperty.NOTIFICATION_FIREBASE_REFERENCE_LOCATION);
    }

    /**
     * Get the Zoom API key.
     * @return The API key.
     */
    public String getZoomApiKey() {
        return getProperty(USDProperty.ZOOM_API_KEY);
    }

    /**
     * Get the Zoom API secret.
     * @return The API secret.
     */
    public String getZoomApiSecret() {
        return getProperty(USDProperty.ZOOM_API_SECRET);
    }

    /**
     * Get whether to run zoom attendance.
     * @return String representation of whether to run.
     */
    public String getScheduledZoomAttendance() {
        return getProperty(USDProperty.SCHEDULED_ZOOM_ATTENDANCE);
    }

    public String getCasUrl() {
        return getProperty(USDProperty.CAS_BASE_URL);
    }
    public String getCasClientId() {
        return getProperty(USDProperty.CAS_CLIENT_ID);
    }
    public String getCasClientSecret() {
        return getProperty(USDProperty.CAS_CLIENT_SECRET);
    }

    public String getNuParkUrl(){
        return getProperty(USDProperty.NU_PARK_BASE_URL);
    }
    public String getNuParkUsername(){
        return getProperty(USDProperty.NU_PARK_USERNAME);
    }
    public String getNuParkPassword(){
        return getProperty(USDProperty.NU_PARK_PASSWORD);
    }

    /* Helpers */

    /**
     * Returns a property for a given propertyName.
     * @param propertyName One of the properties from the property file.
     * @return The value of the property, or null if not found.
     */
    public String getProperty(final String propertyName) {
        Properties prop = new Properties();
        InputStream input;
        try {
            input = new FileInputStream(propFileLocation);
            prop.load(input);
            input.close();
            return prop.getProperty(propertyName);
        } catch (FileNotFoundException e) {
            log.error("Could not find property file: " + e.getMessage());
        } catch (IOException e) {
            log.error("IOException: " + e.getMessage());
        }
        return null;
    }

}
