package edu.sandiego.custom.utils;

/**
 * Created by rdhiman on Sep.26.2018.
 */
final class ERPTAPIDEVProperties {


    /* USD ERPT DEV Related Properties. */

    /** USD_ERPT_DEV_API_BASE_URL. */
    static final String USD_ERPT_DEV_API_BASE_URL = "usd.erpt.api.dev.base-url";
    /** USD_ERPT_DEV_API_VERSION. */
    static final String USD_ERPT_DEV_API_VERSION = "usd.erpt.api.dev.version";
    /** USD_ERPT_DEV_API_SALESFORCE_PATH. */
    static final String USD_ERPT_DEV_API_SALESFORCE_PATH = "usd.erpt.api.dev.salesforce-path";
    /** USD_ERPT_DEV_API_BANNER_PATH. */
    static final String USD_ERPT_DEV_API_BANNER_PATH = "usd.erpt.api.dev.banner-path";
    /** USD_ERPT_DEV_API_FIREBASE_PATH. */
    static final String USD_ERPT_DEV_API_FIREBASE_PATH = "usd.erpt.api.dev.firebase-path";
    /** USD_ERPT_DEV_API_GOOGLE_PATH. */
    static final String USD_ERPT_DEV_API_GOOGLE_PATH = "usd.erpt.api.dev.google-path";
    /** USD_ERPT_DEV_API_TWILIO_PATH. */
    static final String USD_ERPT_DEV_API_TWILIO_PATH = "usd.erpt.api.dev.twilio-path";
    /** USD_ERPT_DEV_API_SENDGRID_PATH. */
    static final String USD_ERPT_DEV_API_SENDGRID_PATH = "usd.erpt.api.dev.sendgrid-path";
    /** USD_ERPT_DEV_API_PUBLIC_PATH. */
    static final String USD_ERPT_DEV_API_PUBLIC_PATH = "usd.erpt.api.dev.public-path";

    /** Prevent this Utility class from being instantiated. */
    private ERPTAPIDEVProperties() {

    }

}
