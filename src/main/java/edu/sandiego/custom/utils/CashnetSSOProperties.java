package edu.sandiego.custom.utils;

/**
 * Created by rdhiman on Dec.20.2018.
 */
final public class CashnetSSOProperties {

    /* CASHNET SSO PROPERTIES */

    /** Cashnet Base URL for SSO. */
    static final String CASHNET_SSO_BASE_URL = "cashnet.base-url";
    /** Cashnet Encryption for SSO. */
    static final String CASHNET_SSO_ENCRYPTION_KEY = "cashnet.sso.encryption-key";

    /** Prevent this Utility class from being instantiated. */
    private CashnetSSOProperties() {

    }

}
